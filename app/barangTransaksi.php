<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barangTransaksi extends Model
{
	protected $table = "barang_transaksi";
	protected $fillable = ['transaksi_id','barang_id','jml','harga'];
	public $timestamps = false;
}

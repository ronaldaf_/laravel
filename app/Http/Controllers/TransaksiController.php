<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaksi;
use App\Customer;
use App\Barang;
use App\barangTransaksi;

class TransaksiController extends Controller
{
    public function index(){
        $transaksi = transaksi::all();

    	// $transaksi = transaksi::with(['customer','detailTransaksi'])->get();
        // $detailTransaksi = detailTransaksi::with(['barang'])->get();
    	// //dengan kondisi
    	// $transaksi = transaksi::where('nama', 'Jamal Uwais')->get();
    	// $transaksi = transaksi::where('id', '>' , 10)->get();
    	// $transaksi = transaksi::where('nama', 'like' , '%a%')->get();
        return view('transaksi', ['transaksi' => $transaksi]);
    }

    public function tambah(){
    	$data_customer = customer::select('id','nama')->get();
        $data_barang = barang::select('id','nama')->get();

    	//select + where
    	//$data_customer = customer::select('id','nama')->where('id','1')->get();
    	//==
    	//SELECT id, nama from customer where id=1;
        return view('transaksi_tambah', ['data_customer' => $data_customer] , ['data_barang' => $data_barang]);
    }

    public function proses(Request $request){
        $this->validate($request,[
            'qty' => 'required'
        ]);

        $barang_id = $request->barang;
        $tbl_barang = barang::select('id','harga')->where('id',$barang_id)->first();
        $harga = $tbl_barang->harga;
        $qty = $request->qty;
        $subtotal = $harga * $qty;
        if($subtotal > 40000){
            $diskon = 2000;
        }else{
            $diskon = 0;
        }
        $total = $subtotal-$diskon;
        $tbl_barang->stok = $tbl_barang->stok-$qty;

        Transaksi::create([
            'customer_id' => $request->customer,
            'subtotal' => $subtotal,
            'diskon' => $diskon,
            'total' => $total
        ]);

        $id_transaksi = Transaksi::latest('id')->first();
        barangTransaksi::create([
            'transaksi_id' => $id_transaksi->id,
            'barang_id' => $tbl_barang->id,
            'jml' => $request->qty,
            'harga' => $tbl_barang->harga
        ]);
        return redirect('/transaksi');
    }

    public function edit($id){
        $data_customer = customer::select('id','nama')->get();
        $data_barang = barang::select('id','nama')->get();
        $transaksi = transaksi::find($id);
        return view('transaksi_edit', ['transaksi' => $transaksi, 'data_customer' => $data_customer] , ['data_barang' => $data_barang]);
    }

    public function update($id, Request $request){
        $this->validate($request,[
            'qty' => 'required'
        ]);
        $barang_id = $request->barang;
        $tbl_barang = barang::select('id','harga')->where('id',$barang_id)->first();
        $harga = $tbl_barang->harga;
        $qty = $request->qty;
        $subtotal = $harga * $qty;
        if($subtotal > 40000){
            $diskon = 2000;
        }else{
            $diskon = 0;
        }
        $total = $subtotal-$diskon;
        $tbl_barang->stok = $tbl_barang->stok-$qty;

        $transaksi = transaksi::find($id);
        $transaksi->customer_id = $request->customer;
        $transaksi->subtotal = $subtotal;
        $transaksi->diskon = $diskon;
        $transaksi->total = $total;

        $detail = barangTransaksi::where('id',$id)->first();
        $detail->transaksi_id = $detail->id;
        $detail->barang_id = $tbl_barang->id;
        $detail->jml = $request->qty;
        $detail->harga = $tbl_barang->harga;
        
        $transaksi->save();
        $detail->save();
        return redirect('/transaksi');
    }

    public function hapus($id){
        $transaksi = transaksi::find($id);
        $detail = barangTransaksi::where('transaksi_id',$id);
        $detail->delete();
        $transaksi->delete();
        return redirect('/transaksi');
    }
}

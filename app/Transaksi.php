<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
	protected $table = "transaksi";
	protected $fillable = ['id','customer_id','subtotal','diskon','total'];
	public $timestamps = false;

	//relasine 
	public function customer(){
		return $this->belongsTo('App\Customer');
	}

	public function barang(){
		return $this->belongsToMany('App\Barang')->withPivot('jml','harga');
	}
}

-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2019 at 03:38 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `harga` double(12,2) NOT NULL,
  `stok` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama`, `harga`, `stok`) VALUES
(1, 'Mie Instan', 1500.00, 20),
(2, 'Sampo Pantene', 1000.00, 50),
(3, 'Sabun Nuvo', 2500.00, 100),
(4, 'Sikat Gigi Pepsodent', 6000.00, 50),
(5, 'Taro', 10000.00, 200),
(6, 'Sepatu Converse', 250000.00, 100);

-- --------------------------------------------------------

--
-- Table structure for table `barang_transaksi`
--

CREATE TABLE `barang_transaksi` (
  `id` int(11) NOT NULL,
  `transaksi_id` int(3) DEFAULT NULL,
  `barang_id` int(3) DEFAULT NULL,
  `jml` int(10) DEFAULT NULL,
  `harga` double(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_transaksi`
--

INSERT INTO `barang_transaksi` (`id`, `transaksi_id`, `barang_id`, `jml`, `harga`) VALUES
(3, 3, 1, 900, 1500.00);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_telp` varchar(30) NOT NULL,
  `catatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama`, `alamat`, `no_telp`, `catatan`) VALUES
(4, 'Joni', 'Jalan Kendalpayak', '0876545672', 'Aku mandiri'),
(5, 'Puji Handayani', 'Gg. Bakau No. 963, Makassar 52374, JaTim', '(+62) 623 3843 180', 'Impedit earum velit voluptates sint accusamus. Aut qui sit pariatur eligendi. Voluptates laborum cupiditate earum sint. Esse dolor occaecati aut sunt aut.'),
(6, 'Jumari Pradana', 'Jln. Wahidin Sudirohusodo No. 508, Sungai Penuh 47044, KepR', '(+62) 228 5956 5205', 'Et cumque rerum nemo quidem est doloribus. Dolor animi consequatur autem fugit velit. Sequi ut illo minima. Vel et quas enim illo sed est.'),
(7, 'Himawan Dabukke S.E.I', 'Kpg. Wahid Hasyim No. 241, Kupang 54075, SulSel', '(+62) 733 8891 965', 'Soluta aperiam totam autem hic provident. Aut sit aut tempore mollitia culpa. Consequatur magni at consequatur commodi architecto et. Provident et hic occaecati repellat rem labore nulla consequatur.'),
(8, 'Hadi Prasetya', 'Ds. Bayan No. 274, Solok 93131, JaBar', '(+62) 589 9907 8828', 'Explicabo eum veritatis rerum fuga eum unde itaque. Nam ipsa ut earum autem ab eum velit suscipit. Voluptate voluptas quia impedit aut tempore. Magnam et veritatis rerum possimus labore.'),
(9, 'Jono Hutapea', 'Kpg. Otista No. 589, Parepare 12982, Aceh', '(+62) 685 3971 610', 'Dolores pariatur molestiae officiis animi inventore. Architecto hic nesciunt non. Eligendi reiciendis non voluptatem accusantium. Ut inventore quia sed sed quos mollitia.'),
(10, 'Wirda Wastuti', 'Jr. Tubagus Ismail No. 988, Cirebon 55384, KalSel', '0802 703 314', 'Rerum aut dignissimos ut et qui ratione omnis. Quaerat quia aspernatur maiores nesciunt aut. Nihil enim voluptas aspernatur cum rem. Dicta debitis laboriosam delectus et rerum natus quos.'),
(11, 'Martana Pranowo S.Kom', 'Ki. Kyai Mojo No. 671, Sabang 87168, JaBar', '(+62) 417 6752 994', 'Voluptatibus sunt dolorum eum laborum earum cupiditate adipisci. Sit aut placeat rerum laborum dolorem. Autem iusto perspiciatis nobis eligendi est ratione.'),
(12, 'Violet Oliva Nasyiah S.Farm', 'Psr. Ahmad Dahlan No. 643, Banjarmasin 86563, NTT', '0908 2539 729', 'Cumque illo alias ea quis. Veritatis assumenda molestiae repellendus et et veritatis. Nihil placeat inventore ut impedit. Voluptatibus non explicabo aut fuga id.'),
(13, 'Laras Suartini', 'Dk. Kalimantan No. 857, Sabang 26163, KalSel', '(+62) 793 5523 4297', 'Dolorem iure recusandae exercitationem odit. Tempore et molestiae et voluptatibus beatae. Cumque omnis beatae ut doloribus iure consequatur. Earum qui sint id.'),
(14, 'Bakda Dasa Permadi S.IP', 'Jln. Bahagia  No. 126, Jambi 67867, Riau', '(+62) 901 6116 9969', 'Maxime at a quasi et quis. Molestiae tempora saepe dolor saepe amet qui amet. Earum natus facilis dolores suscipit fugiat odit consequatur. Et corrupti sapiente exercitationem et sit earum libero.'),
(15, 'Timbul Gaduh Adriansyah', 'Jln. Kali No. 556, Madiun 54197, DKI', '0278 4892 324', 'Ut cum voluptate quibusdam non accusantium nulla. Facere quo odit maxime enim esse dolorem ipsam. Velit repellendus tempore voluptate deserunt rerum.'),
(16, 'Kamaria Mardhiyah', 'Dk. Bak Air No. 390, Pekanbaru 55533, SumSel', '(+62) 422 8321 6108', 'Esse sit velit quidem et consequatur consequatur et. Sint vel sed et blanditiis. Dolorem voluptas libero eaque fugit exercitationem. Ea laborum et ad voluptatem.'),
(17, 'Maida Hastuti S.Psi', 'Psr. R.M. Said No. 366, Ternate 51339, JaTeng', '0741 1181 1937', 'Voluptatem quasi sed nihil ipsum et. Odio ratione temporibus odit et sint molestias. Tempore aut unde expedita magni est error. Molestiae commodi accusantium nihil deserunt odio dolores eaque.'),
(18, 'Syahrini Kusmawati S.Kom', 'Jln. Camar No. 122, Palembang 48452, SulTeng', '0640 4159 8617', 'Odit eum in voluptas eaque omnis cupiditate earum. Molestias laborum qui impedit ab hic voluptas.'),
(19, 'Jamalia Maryati', 'Dk. Bakau Griya Utama No. 795, Bogor 93276, KalTeng', '(+62) 999 8640 150', 'Temporibus iste id quae perspiciatis. Recusandae eos sequi earum. Sunt fugiat perferendis reiciendis cupiditate praesentium doloribus aut.'),
(20, 'Kasiran Eja Kuswoyo S.E.I', 'Gg. Bakau Griya Utama No. 90, Kotamobagu 25493, KalBar', '(+62) 27 9921 2197', 'Inventore fuga odio expedita temporibus. Nisi aliquid velit dolor suscipit. Quae ullam aliquam est vero officia soluta.'),
(21, 'Tina Gilda Novitasari', 'Psr. Raden Saleh No. 437, Madiun 82293, SulTeng', '0507 3702 340', 'Inventore corrupti facere nam amet dignissimos. Harum ratione nulla autem assumenda aspernatur ducimus et. Rerum consectetur dolorem dolor rerum.'),
(22, 'Zelaya Azalea Anggraini S.Sos', 'Jr. Otto No. 249, Makassar 64234, NTT', '0891 2519 918', 'Qui ut sunt et ducimus culpa odit adipisci ducimus. Sit esse molestias dicta quas. Itaque perferendis recusandae ab id. Numquam perferendis qui sed molestias deserunt qui.'),
(23, 'Agnes Melani', 'Jr. Sukabumi No. 356, Ambon 16806, SulBar', '(+62) 719 0718 618', 'Alias quia aspernatur ullam et accusamus dolor ut. Velit ut autem qui autem cum temporibus voluptatem. Consequatur cumque nobis sit unde aut ut est omnis.'),
(24, 'Mutia Aryani', 'Kpg. Kiaracondong No. 141, Metro 47074, KalTeng', '(+62) 25 3727 516', 'Quo minima quas aut. Ut veritatis adipisci veniam necessitatibus porro et qui a. Vero eum enim repellendus unde nihil. Occaecati velit autem quia dolore aliquid nemo sed.'),
(25, 'Balidin Napitupulu M.M.', 'Jln. Nanas No. 529, Sibolga 12731, Bali', '(+62) 28 3016 426', 'Harum praesentium molestiae cumque sunt sunt quisquam accusantium magnam. Est sint doloremque quis amet.'),
(26, 'Imam Waskita', 'Jln. Moch. Yamin No. 882, Bukittinggi 67821, Banten', '0461 5023 7431', 'Ad repellendus occaecati magnam corporis. Delectus aut perferendis veritatis corporis. Eum error illum quia expedita doloremque. At aliquid tenetur ipsa cumque voluptatem.'),
(27, 'Kamila Oktaviani', 'Gg. Raden Saleh No. 266, Sungai Penuh 62009, NTB', '0813 768 736', 'Voluptatem laborum labore doloribus ut. Porro qui quia totam est ea. Dolor laborum autem et.'),
(28, 'Hamima Lestari', 'Psr. Pasirkoja No. 408, Ambon 66981, Bali', '(+62) 220 4756 3058', 'Incidunt magnam est quam. Non magni fugiat dolorum tempore tempore magni tenetur. Et non velit qui aut ut quidem ut harum. Vitae labore eos non consectetur.'),
(29, 'Eli Almira Wulandari S.T.', 'Ki. Juanda No. 479, Tegal 57117, JaTeng', '(+62) 742 1577 3804', 'Est corporis debitis minima enim dicta. Autem vel numquam et sint quis. Qui reprehenderit et laborum non facilis excepturi voluptate.'),
(30, 'Artanto Uwais', 'Psr. Salatiga No. 912, Balikpapan 55669, Riau', '0382 4961 3169', 'Voluptas omnis et dolorem et. Hic optio veniam eos soluta repudiandae quis.'),
(31, 'Padma Restu Yulianti', 'Gg. Padma No. 646, Metro 91188, SumBar', '(+62) 500 3962 2040', 'Et omnis ipsa sed hic fugiat. Amet cum sequi aliquam eos inventore et et neque. Repellat voluptates a ut ratione.'),
(32, 'Darijan Wijaya S.Farm', 'Jln. Raden No. 408, Serang 31477, Bengkulu', '(+62) 643 3877 4261', 'Maxime eum quia est et est. Explicabo veniam vitae minus voluptatem atque dolorem unde consequatur. Beatae labore voluptatem culpa perspiciatis. Ut placeat quibusdam distinctio animi ad eius.'),
(33, 'Sadina Handayani', 'Jln. Abang No. 853, Tarakan 16926, Banten', '0868 3323 748', 'Dolorem vero eaque pariatur consequatur quis. Harum dolorum aut numquam vero. Explicabo nisi sapiente voluptatem sunt quod.'),
(34, 'Jabal Santoso S.Farm', 'Jln. Dipatiukur No. 933, Tual 11751, NTB', '0655 6119 5815', 'Accusamus error error voluptatem enim facere. Id consectetur accusamus tempora at assumenda. Voluptatem voluptas reiciendis recusandae at ab qui. Quis et sunt ratione aut.'),
(35, 'Carub Warsita Gunawan S.Pt', 'Jr. Madiun No. 678, Jayapura 93781, MalUt', '(+62) 23 6988 814', 'Consequatur cumque et blanditiis voluptates quasi et earum. Itaque dicta optio eos et.'),
(36, 'Vivi Handayani', 'Jln. Kyai Gede No. 606, Bitung 55446, KalUt', '(+62) 615 1375 5158', 'Enim culpa atque quibusdam molestiae sit doloribus. Maxime rerum nemo sed vel et animi dolorum. Qui dolorem temporibus id. Quia eum consequatur provident laudantium in harum temporibus.'),
(37, 'Farah Mulyani', 'Psr. Peta No. 858, Surabaya 97385, Bali', '(+62) 850 9839 7283', 'Ea aut nihil libero veniam maiores et. Dolorem cupiditate doloribus harum porro quas repellat. Consequatur reprehenderit rerum culpa odit totam quia eveniet expedita. Ducimus nisi ad odit et.'),
(38, 'Dodo Mandala', 'Kpg. Bakit  No. 505, Padangpanjang 55879, MalUt', '(+62) 23 1463 6788', 'Exercitationem et odio quis consectetur. Quia adipisci vero voluptatem dignissimos itaque. Omnis necessitatibus autem sed dolorem accusamus explicabo quis.'),
(39, 'Ira Yuliarti', 'Jln. Orang No. 725, Madiun 61415, KalUt', '(+62) 796 4828 942', 'Consequuntur veniam non error. Laudantium ut iure voluptas ullam. Voluptatibus ipsam omnis eaque nisi.'),
(40, 'Ifa Wastuti', 'Psr. Baha No. 226, Tomohon 51135, SulBar', '0835 3597 234', 'Facilis beatae soluta dolores. Quasi sit dolorem eos. Aut veniam hic repudiandae dolor eveniet earum iusto nobis.'),
(41, 'Danang Wahyudin M.Ak', 'Psr. Casablanca No. 588, Administrasi Jakarta Selatan 85850, Bali', '(+62) 562 1188 405', 'Qui ducimus fugiat sint possimus quasi placeat quis. Et vitae omnis sequi ipsam. Quia dolor et vel nulla ut quibusdam ipsam. Vel doloribus recusandae eius adipisci.'),
(42, 'Titin Yulianti S.Kom', 'Jr. Abdul Rahmat No. 679, Tual 72888, KalUt', '0336 2227 8583', 'Totam ut quas laudantium non. Ipsam magni hic iusto eaque. Est sed beatae provident magnam hic architecto vel.'),
(43, 'Padma Anita Nasyiah M.Farm', 'Jr. Babadan No. 818, Tomohon 15621, SumUt', '(+62) 800 7870 9125', 'Ea temporibus perspiciatis impedit sed nihil aliquam. Consectetur molestias sint aut doloremque. Unde molestias aspernatur tempora iure.'),
(44, 'Tantri Wulan Susanti S.I.Kom', 'Psr. Basuki Rahmat  No. 275, Sukabumi 83486, KalBar', '(+62) 739 2924 4632', 'Corrupti consectetur explicabo itaque cumque et. Dolorem ipsam reprehenderit sed odit. Quam voluptas vel aut magnam consequatur maiores.'),
(45, 'Teguh Prabowo', 'Ds. Samanhudi No. 166, Pematangsiantar 28089, SulTeng', '(+62) 758 9448 7570', 'Dolores a illo dolores veritatis culpa. Vero sunt facere ut. Mollitia deserunt quia a rerum aut aut.'),
(46, 'Raina Nurdiyanti', 'Jln. Baranang No. 799, Solok 40154, Riau', '029 5146 7989', 'Et dolorum quibusdam ut repudiandae ipsum voluptatem quia. Tempora non atque culpa in eius dolorum voluptas quis. Mollitia doloribus odio error debitis illum.'),
(47, 'Darmanto Kasiran Manullang', 'Jln. Cikutra Barat No. 283, Sungai Penuh 74189, BaBel', '(+62) 728 2477 663', 'Exercitationem consequatur aperiam omnis dolores veritatis. Tempore aliquam voluptas et non necessitatibus non iusto. Eveniet soluta aut id rem fuga et. Nam est dolorem assumenda tempora minima.'),
(48, 'Artawan Hutapea M.TI.', 'Psr. Suprapto No. 926, Ternate 52636, SumUt', '(+62) 449 9924 4955', 'Repudiandae labore quo eum reprehenderit. Suscipit necessitatibus et aut voluptatum doloribus qui. Unde beatae nulla dicta soluta voluptatem.'),
(49, 'Ghaliyati Nasyidah', 'Ki. Kebonjati No. 895, Bandar Lampung 32478, Banten', '(+62) 382 8693 8359', 'Ut rem et earum quia quia commodi praesentium quae. Dignissimos aspernatur autem sequi consectetur. Quaerat blanditiis sint voluptas rerum eaque et consectetur.'),
(50, 'Olivia Puspita', 'Jln. Ters. Kiaracondong No. 876, Administrasi Jakarta Barat 69552, JaTim', '(+62) 311 8955 724', 'Totam provident eum sed excepturi aut culpa et. Maiores sint dolores tempora assumenda sint voluptatem impedit dolorum. Et voluptatem quis optio.'),
(51, 'Paris Nuraini', 'Dk. Baung No. 540, Solok 75194, PapBar', '0300 9441 1167', 'Architecto illum fuga ullam aut voluptatem. Ab iste praesentium magni sed nesciunt est blanditiis. Et fugiat accusantium tempore quaerat illum. Consequatur dolorum quod provident maxime repudiandae.'),
(52, 'Aris Kenes Nashiruddin', 'Ds. Pasteur No. 912, Cilegon 41635, Banten', '022 5345 863', 'Hic possimus reiciendis quasi est vel ad. Officiis commodi tempora fugiat. Sunt quibusdam quis aspernatur rerum et debitis quam. Adipisci tempore nihil consequatur explicabo.'),
(53, 'Bagas Kuswoyo', 'Jln. Flora No. 346, Serang 69311, Gorontalo', '0248 0422 825', 'Odit architecto qui sunt velit. Quas illum voluptatem distinctio accusamus. Ut repellat provident voluptas qui et distinctio. Eos ab non cupiditate et sapiente quisquam maiores.'),
(54, 'Hana Salsabila Hasanah', 'Kpg. Baladewa No. 204, Balikpapan 22328, Lampung', '028 2314 7023', 'Cumque illum error est. Ut ut sint explicabo placeat quo fugiat. Alias velit error ea modi voluptas. Et voluptas veniam dolorem dolor. Natus iure occaecati doloremque.'),
(55, 'Aku Dewe 123', 'Jalanin AJA', '0821635213', 'Aku'),
(56, 'ssdsadsadasdsad', 'asd', '2', 'asda'),
(57, 'RenaPutriDiana', 'Jalanin aja dulu', '082362132', 'Aku');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(3) NOT NULL,
  `customer_id` int(3) DEFAULT NULL,
  `subtotal` double(12,2) DEFAULT NULL,
  `diskon` double(12,2) DEFAULT NULL,
  `total` double(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `customer_id`, `subtotal`, `diskon`, `total`) VALUES
(3, 4, 1348000.00, 2000.00, 1346000.00);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Saya', 'saya@saya.com', NULL, '$2y$10$0V.vCTmOG2tb3JTQ6qTQR.Ny1GMjfuce57nBhkwfFqmRqL5JcDsM.', NULL, '2019-07-25 23:41:54', '2019-07-25 23:41:54'),
(2, 'wer', '1232@gmail.com', NULL, '$2y$10$/v5VWcYQZb3bQDQLwdTpSODDjE/y3OKjy1fbw1P.lYi0b6BDOlVBy', NULL, '2019-07-26 00:53:27', '2019-07-26 00:53:27'),
(3, 'Ah', 'ahha@123.com', NULL, '$2y$10$o0wqA.qKDerupCVq6tlWjevy8ETI3eRsFcewakaHw7cbsgp9kQIJ6', NULL, '2019-07-26 02:04:16', '2019-07-26 02:04:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang_transaksi`
--
ALTER TABLE `barang_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detailtransaksi_transaksi` (`transaksi_id`),
  ADD KEY `fk_detailtransaksi_barang` (`barang_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transaksi_customer` (`customer_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `barang_transaksi`
--
ALTER TABLE `barang_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang_transaksi`
--
ALTER TABLE `barang_transaksi`
  ADD CONSTRAINT `fk_detailtransaksi_barang` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`),
  ADD CONSTRAINT `fk_detailtransaksi_transaksi` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `fk_transaksi_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

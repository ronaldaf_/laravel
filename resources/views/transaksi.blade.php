<!-- Menghubungkan dengan view template master -->
@extends('master')
<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->
@section('judul_halaman', 'Halaman Transaksi')
<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('konten')
<center><h4>Data Transaksi</h4></center>
@foreach($transaksi as $trx)
<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="card mt-4">
			<div class="card-body">
				Yth. {{ $trx->customer['nama'] }}
				<br>
				Alamat {{ $trx->customer['alamat'] }}
				<br>
				No Telp {{ $trx->customer['no_telp'] }}
				<br>
			</div>
			<div class="row">
				<div class="col-md-2" style="margin-left: 20px;">
					<a href="/transaksi/edit/{{ $trx->id }}" class="fa fa-edit btn btn-warning" > Edit</a>
				</div>
				<div class="col-md-2" style="margin-left: -40px;">
					<a href="/transaksi/hapus/{{ $trx->id }}" class="fa fa-times btn btn-danger"> Hapus</a>
				</div>
			</div>
			<div class="row" style="padding-bottom: 30px;padding-top: 20px;margin-left: 5px">
				<div class="col-md-2">
					<a class="detail fa fa-chevron-right btn btn-success" id="detail-{{ $trx->customer_id }}" style="width:155px;color:white;"> Detail</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="card">
			<div class="card-body" style="background-color: #ececec;">
				Barang : <br> 
				@foreach($trx->barang as $brg)
				 - {{ $brg->nama }} {{ $brg->pivot->jml }} buah
				<br> 
				@endforeach
				Sub Total : Rp. {{ $trx->subtotal }}
				<br>
				Diskon : Rp. {{ $trx->diskon }}
				<br>
				Total : Rp. {{ $trx->total }}
				<br>
			</div>
		</div>
	</div>
</div>
@endforeach
<a href="/transaksi/tambah" class="float">
	<i class="fa fa-plus my-float"></i>
</a>
@endsection